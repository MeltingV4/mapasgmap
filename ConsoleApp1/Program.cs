﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SbsSW.SwiPlCs;

namespace ConsoleApp1
{
    class Program
    {

        static void Main(string[] args)
        {
            if (!PlEngine.IsInitialized)
            {
                try
                {

                    string dt1="", dt2="";

                    Console.WriteLine("Escribe el origen: ");
                    dt1 = Console.ReadLine();

                    Console.WriteLine("Escribe el destino: ");
                    dt2 = Console.ReadLine();

                    //Prueba de conexion con Prolog
                    Environment.SetEnvironmentVariable("SWI_HOME_DIR", @"C:\\Program Files (x86)\\swipl");
                    Environment.SetEnvironmentVariable("Path", @"C:\\Program Files (x86)\\swipl\\bin");

                    string[] p = { "-q", "-f", @"agenteviajes.pl" };
                    PlEngine.Initialize(p);
                    PlQuery consult;
                    
                    PlQuery cargar = new PlQuery("cargar('agenteviajes.bd')");
                    //PlQuery cargar = new PlQuery("cargar('sumas.bd')");
                    cargar.NextSolution();


                    string ruta;

                    consult = new PlQuery("buscar_ruta_amplitud("+dt1+","+dt2+",C).");
                    //consult = new PlQuery("suma(3,4,C)");
                    if (consult.NextSolution())
                    {
                        foreach (PlQueryVariables item in consult.SolutionVariables)
                        {
                            ruta=item["C"].ToString();
                        }
                        PlEngine.PlCleanup();
                    }
                    else
                    {
                        Console.WriteLine("No se encuentra la ruta");
                    }


                    
                }
                catch (Exception e)
                {

                    Console.WriteLine(e.Message);
                }
                Console.ReadKey();
            }
        }
    }
}
