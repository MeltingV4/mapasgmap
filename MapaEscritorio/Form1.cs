﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using SbsSW.SwiPlCs;

namespace MapaEscritorio
{
    public partial class Form1 : Form
    {
        GMarkerGoogle marker;
        GMapOverlay mapOverlay;
        DataTable dt;
        int filaSeleccionada = 0;
        double LatInicial = 23.5706101;
        double LngInicial = -105.628593;

        public Form1()
        {
            InitializeComponent();
        }

        private void  From1_Load(object sender,EventArgs e)
        {
            gMapControl1.DragButton = MouseButtons.Left;
            gMapControl1.CanDragMap = true;
            gMapControl1.MapProvider = GMapProviders.GoogleMap;
            gMapControl1.Position = new PointLatLng(LatInicial,LngInicial);
            gMapControl1.MinZoom = 3;
            gMapControl1.MaxZoom=24;
            gMapControl1.Zoom = 5;
            gMapControl1.AutoScroll = true;
        }

        private void btnRuta_Click(object sender, EventArgs e)
        {

            string rutaa = "" ;

            if (!PlEngine.IsInitialized)
            {
                try
                {

                    string dt1 = "", dt2 = "";

                    dt1 = txorigen.Text;
                    dt2 = txdestino.Text;

                    //Prueba de conexion con Prolog
                    Environment.SetEnvironmentVariable("SWI_HOME_DIR", @"C:\\Program Files (x86)\\swipl");
                    Environment.SetEnvironmentVariable("Path", @"C:\\Program Files (x86)\\swipl\\bin");

                    string[] p = { "-q", "-f", @"agenteviajes.pl" };
                    PlEngine.Initialize(p);
                    PlQuery consult;
                    PlQuery cargar = new PlQuery("cargar('agenteviajes.bd')");
                    cargar.NextSolution();
                    
                    consult = new PlQuery("buscar_ruta_amplitud(" + dt1 + "," + dt2 + ",X).");
                    if (consult.NextSolution())
                    {
                        foreach (PlQueryVariables item in consult.SolutionVariables)
                        {
                            rutaa = item["X"].ToString();
                        }
                        PlEngine.PlCleanup();
                    }
                    else
                    {
                        Console.WriteLine("No se encuentra la ruta");
                    }

                    Console.WriteLine(rutaa);

                    dt = new DataTable();
                    dt.Columns.Add(new DataColumn("Lugar", typeof(string)));
                    dt.Columns.Add(new DataColumn("Lat", typeof(string)));
                    dt.Columns.Add(new DataColumn("Lng", typeof(string)));

                    int posision = 0;

                    string[] rutas = { "campeche", "merida", "tizimin", "valladolid", "cancun" };

                    //ruta por lista
                    double[,] lista = new double[rutas.Length, 2];

                    //Lista de datos
                    Dictionary<string, string> lugares = new Dictionary<string, string>();
                    lugares.Add("jalpa_de_mendez", "18.1143023,-93.0416424");
                    lugares.Add("pto_ceiba", "18.4059552,-93.207365");
                    lugares.Add("paraíso", "18.4007798,-93.2099189");
                    lugares.Add("comalcalco", "18.2659733,-93.2224492");
                    lugares.Add("nacajuca", "18.0071693,-92.9344089");
                    lugares.Add("macuspana", "17.7622378,-92.6109769");
                    lugares.Add("teapa", "17.5480526,-92.9557826");
                    lugares.Add("pichucalco", "17.5563755,-93.0363961");
                    lugares.Add("ocosingo", "16.9084903,-92.1266804");
                    lugares.Add("comitan", "16.2507372,-92.1324987");
                    lugares.Add("frontera_comalapa", "15.6632474,-92.1423206");
                    lugares.Add("motozintla", "15.6486426,-92.1417957");
                    lugares.Add("talisman", "14.8751754,-92.2556561");
                    lugares.Add("tapachula", "14.9133638,-92.2599798");
                    lugares.Add("huixtla", "15.1376603,-92.5009387");
                    lugares.Add("escuintla", "15.3232476,-92.6936361");
                    lugares.Add("mapastepec", "15.4329655,-92.9328597");
                    lugares.Add("pijijiapan", "15.6851681,-93.2472734");
                    lugares.Add("tonala", "16.0869347,-93.8313261");
                    lugares.Add("arriaga", "16.237218,-93.9336476");
                    lugares.Add("san_cristobal_de_las_casas", "16.7371151,-92.6726383");
                    lugares.Add("tuxtla_gutierrez", "16.7516801,-93.1380136");
                    lugares.Add("cardenas", "17.9908235,-93.4116133");
                    lugares.Add("cunduacan", "18.0722328,-93.2062683");
                    lugares.Add("reforma", "17.858012,-93.1898288");
                    lugares.Add("huimanguillo", "17.8321227,-93.4268303");
                    lugares.Add("chontalpa", "17.665058,-93.5147949");
                    lugares.Add("raudales_de_malpaso", "17.1899615,-93.6390413");
                    lugares.Add("agua_dulce", "18.1368302,-94.153016");
                    lugares.Add("la_venta", "18.0988737,-94.0472238,17");
                    lugares.Add("las_choapas", "17.9157513,-94.1280566");
                    lugares.Add("cintalapa", "16.6889916,-93.7234452,17");
                    lugares.Add("coatzacoalcos", "18.1208204,-94.4469749,17");
                    lugares.Add("cuichapa", "18.7729965,-96.8700698,18");
                    lugares.Add("tapanatepec", "16.3636312,-94.2006008,15");
                    lugares.Add("ixhuatan", "16.3527173,-94.4858564,17");
                    lugares.Add("nanchital", "18.0708901,-94.4106825,17");
                    lugares.Add("minatitlan", "17.987848,-94.5545519,16");
                    lugares.Add("cosoleacaque", "17.9970321,-94.6414426,17");
                    lugares.Add("jaltipan", "17.9583718,-94.7113547,14");
                    lugares.Add("unión_hidalgo", "6.472338,-94.8317266");
                    lugares.Add("juchitan", "16.4451471,-95.0273832");
                    lugares.Add("salina_cruz", "16.1846701,-95.201473");
                    lugares.Add("ixtaltepec", "16.504215,-95.0639506");
                    lugares.Add("cd_ixtepec", "16.5566231,-95.0957476");
                    lugares.Add("lagunas", "16.7998476,-95.107902");
                    lugares.Add("tehuantepec", "116.3392009,-95.2268332");
                    lugares.Add("jalapa_del_marquez", "16.4386886,-95.4448104");
                    lugares.Add("acayucan", "17.9480999,-94.9068335");
                    lugares.Add("juan_rodriguez_clara", "17.994764,-95.4008917");
                    lugares.Add("maria_lombardo", "17.4478619,-95.4282366");
                    lugares.Add("playa_vicente", "17.8283058,-95.8181546");
                    lugares.Add("catemaco", "18.417309,-95.1066475");
                    lugares.Add("san_andres_tuxtla", "18.4516173,-95.2207857");
                    lugares.Add("santiago_tuxtla", "18.462352,-95.289832");
                    lugares.Add("villa_islas", "18.0294572,-95.5437439");
                    lugares.Add("pochutla", "15.7446628,-96.4671531");
                    lugares.Add("oaxaca", "17.0703518,-96.7172861");
                    lugares.Add("tlaxcala", "19.4182933,-98.4471175");
                    lugares.Add("pto_escondido", "15.8641356,-97.0690735");
                    lugares.Add("pto_angel", "15.7426594,-96.4689973");
                    lugares.Add("cd_lerdo", "25.5576231,-103.515662");
                    lugares.Add("angel_r_cabada", "18.5969117,-95.4457416");
                    lugares.Add("cosamaloapan", "18.3644082,-95.7944251");
                    lugares.Add("alvarado", "18.7746795,-95.7648379");
                    lugares.Add("veracruz", "19.1824638,-96.2092569");
                    lugares.Add("vega_de_la_torre", "20.0302719,-96.6466876");
                    lugares.Add("tlapacoyan", "19.9632326,-97.2157812");
                    lugares.Add("tulancingo", "20.0994414,-98.3572557");
                    lugares.Add("pachuca", "20.0604904,-98.7757246");
                    lugares.Add("huauchinango", "20.1789838,-98.052657");
                    lugares.Add("villa_juarez", "19.0524295,-98.2268341");
                    lugares.Add("mtz_de_la_torre", "20.0644927,-97.0529068");
                    lugares.Add("san_rafael", "20.1898986,-96.8664896");
                    lugares.Add("papantla", "20.4512632,-97.3199566");
                    lugares.Add("nautla", "20.2097284,-96.7759738");
                    lugares.Add("costa_esmeralda", "20.3380406,-96.8835763");
                    lugares.Add("tecolutla", "20.4788369,-97.0091077");
                    lugares.Add("gtz_Zamora", "20.4459814,-97.0923169");
                    lugares.Add("zacualtipan", "20.4335825,-98.3495958");
                    lugares.Add("molongo", "20.7861013,-98.7254774");
                    lugares.Add("tlalchinol", "20.9882291,-98.662552");
                    lugares.Add("huejutla", "21.1461103,-98.419006");
                    lugares.Add("chicontepec", "20.9710175,-98.1695909");
                    lugares.Add("poza_rica", "20.5250198,-97.4632556");
                    lugares.Add("tuxpan", "20.9634938,-97.4186693");
                    lugares.Add("alamo", "20.9092208,-97.6951626");
                    lugares.Add("cerro_azul", "21.1896099,-97.7514449");
                    lugares.Add("naranjos", "21.353347,-97.6848747");
                    lugares.Add("tantoyuca", "21.346943,-98.2277375");
                    lugares.Add("platon_sanchez", "21.2743433,-98.3826704");
                    lugares.Add("ozuluama", "21.6997319,-98.0536902");
                    lugares.Add("panuco", "22.0468547,-98.1925284");
                    lugares.Add("tampico", "22.2490623,-97.8606583");
                    lugares.Add("altamira", "22.4080386,-97.9539444");
                    lugares.Add("soto_la_marina", "23.7674114,-98.2245122");
                    lugares.Add("san_fernando", "24.8491369,-98.1558927");
                    lugares.Add("matamoros", "25.8713978,-97.5005167");
                    lugares.Add("brownsville", "25.8713286,-97.5125144,14");
                    lugares.Add("reynosa", "26.0827024,-98.2784167");
                    lugares.Add("mc_allen", "26.0827784,-98.2784167");
                    lugares.Add("tlacotalpan", "18.613444,-95.6701059");
                    lugares.Add("san_cristobal", "16.7336724,-92.6408888");
                    lugares.Add("amatitlan", "14.5462652,-90.6625245");
                    lugares.Add("loma_bonita", "18.1392284,-95.880097");
                    lugares.Add("santa_cruz", "18.1633202,-96.0832186");
                    lugares.Add("valle_nacional", "17.7931695,-96.2622378");
                    lugares.Add("san_lucas_ojitlan", "18.0593644,-96.3958882");
                    lugares.Add("tuxtepec", "18.0849801,-96.1278429");
                    lugares.Add("tres_valles", "18.2420929,-96.1363486");
                    lugares.Add("tierra_blanca", "18.4495609,-96.393261");
                    lugares.Add("la_tinaja", "18.8888515,-96.9367343");
                    lugares.Add("cordoba", "18.889663,-96.9400443");
                    lugares.Add("jalapa_de_diaz", "18.0715534,-96.5397299");
                    lugares.Add("pinotepa_Nal", "16.3533109,-98.0699559");
                    lugares.Add("cacahuatepec", "16.6086701,-98.1611448");
                    lugares.Add("san_pedro_amuzgos", "16.6535089,-98.0895008");
                    lugares.Add("sta._ma.zacatepec", "19.0081176,-98.2454907");
                    lugares.Add("putla", "17.0243314,-97.9276845");
                    lugares.Add("tlaxiaco", "17.2659587,-97.6771133");
                    lugares.Add("yucuxaco", "17.2659538,-97.6792982");
                    lugares.Add("yolomecatl", "17.8112982,-97.777722");
                    lugares.Add("nochixtlan", "17.460355,-97.2330639");
                    lugares.Add("santiago_tejupan", "17.662236,-97.4762355");
                    lugares.Add("tamazulapan", "17.672311, -97.572057");
                    lugares.Add("san_felipe", "31.025038, -114.841034");
                    lugares.Add("teposcolula", "17.510811, -97.488327");
                    lugares.Add("juxtlahuaca", "17.332768, -98.013354");
                    lugares.Add("huajuapan_de_león", "17.811665, -97.775547");
                    lugares.Add("Acapulco", "16.862546, -99.901058");
                    lugares.Add("coyuca_de_benitez", "17.008539, -100.089600");
                    lugares.Add("tlapa", "17.548435, -98.573185");
                    lugares.Add("alpoyeca", "17.671563,_-98.509852");
                    lugares.Add("huamuxtitlan", "17.805729, -98.558885");
                    lugares.Add("xochihuehuetlan", "17.901391, -98.487012");
                    lugares.Add("acatlan", "18.200798, -98.048756");
                    lugares.Add("chazumba", "18.190828, -97.678687");
                    lugares.Add("tehuitzingo", "18.333243,_-98.276870");
                    lugares.Add("teotitlan_del_camino", "18.130782,-97.072636");
                    lugares.Add("huautla_de_jimenez", "18.127074,-96.837070");
                    lugares.Add("temascal", "18.2380826,-96.4090207");
                    lugares.Add("tehuacan", "18.4609805,-97.3944497");
                    lugares.Add("fortin_de_las_flores", "18.9001414,-97.0019967");
                    lugares.Add("cd_mendoza", "18.808531,-97.1760579");
                    lugares.Add("orizaba", "18.8472639,-97.0997986");
                    lugares.Add("paso_de_ovejas", "19.280771,-96.434851");
                    lugares.Add("chocaman", "19.012641,-97.028978");
                    lugares.Add("cd_cardel", "19.370648,-96.379831");
                    lugares.Add("chachalacas", "19.419532,-96.325373");
                    lugares.Add("zempoala", "19.444293,-96.409039");
                    lugares.Add("palma_sola", "19.770816,-96.431547");
                    lugares.Add("rinconada", "19.354466,-96.565301");
                    lugares.Add("jalcomulco", "19.332059, -96.761893");
                    lugares.Add("huatusco", "19.150550, -96.965417");
                    lugares.Add("atoyac", "18.914354, -96.777906");
                    lugares.Add("tecpan_de_galeana", "17.222527, -100.631429");
                    lugares.Add("petatlan", "17.539664, -101.270833");
                    lugares.Add("ixtapa", "17.637038, -101.563840");
                    lugares.Add("lazaro_cardenas", "17.637038, -101.563840");
                    lugares.Add("chilpancingo", "17.556777, -99.506077");
                    lugares.Add("cd_altamirano", "18.356033, -100.665219");
                    lugares.Add("arcelia", "18.316803, -100.282969");
                    lugares.Add("iguala", "18.345304, -99.537495");
                    lugares.Add("taxco", "18.558142, -99.604417");
                    lugares.Add("san_juan_ixcaquixtla", "18.461480, -97.831194");
                    lugares.Add("tlacotelpec", "18.6748317,-97.651876");
                    lugares.Add("molcaxac", "19.086562, -98.237195");
                    lugares.Add("tepexi_de_rodriguez", "19.086506, -98.237162");
                    lugares.Add("izucar_de_matamoros", "19.074749, -98.206669");
                    lugares.Add("tepexco", "19.073931, -98.204856");
                    lugares.Add("cuautla", "18.823816, -98.947967");
                    lugares.Add("cuautlixco", "18.845713, -98.940445");
                    lugares.Add("c_de_yecapixtla", "18.8838028,-98.8650719");
                    lugares.Add("oaxtepec", "18.900267, -98.978253");
                    lugares.Add("tepetlixta", "19.034147, -98.797058");
                    lugares.Add("amecameca", "19.130178, -98.769732");
                    lugares.Add("chalco", "19.130280, -98.770266");
                    lugares.Add("tepoztlan", "18.982040, -99.107742");
                    lugares.Add("yautepec", "18.887314, -99.058742");
                    lugares.Add("cuernavaca", "18.928540, -99.231271");
                    lugares.Add("oacalco", "18.924821, -99.031817");
                    lugares.Add("mexico_df", "19.4807191,-99.1405331,17");
                    lugares.Add("oriental", "19.4807292,-99.1405331,17");
                    lugares.Add("libres", "19.4643668,-97.6871064,17");
                    lugares.Add("apizaco", "19.4138081,-98.1438313,17");
                    lugares.Add("zaragoza", "19.7699714,-97.5547002,17");
                    lugares.Add("coatepec", "19.4527263,-96.960032,16");
                    lugares.Add("perote", "19.5623131,-97.2457776,17");
                    lugares.Add("jalapa", "19.5035799,-97.0074181,12.5");
                    lugares.Add("misantla", "19.9272173,-96.8573916,19");
                    lugares.Add("emilio_carranza", "19.9688454,-96.6129853,17");
                    lugares.Add("tlatlauquitepec", "19.8450272,-97.4998628,17");
                    lugares.Add("teziutlan", "19.8175176,-97.3605535,17");
                    lugares.Add("altoltonga", "19.767348,-97.2456886,17");
                    lugares.Add("cancun", "21.1040182,-86.9223335");
                    lugares.Add("playa_del_carmen", "20.6227966,-87.0774164");
                    lugares.Add("tulum", "20.2106103,-87.4651132");
                    lugares.Add("tizimin", "21.1448295,-88.1510586");
                    lugares.Add("valladolid", "20.6908861,-88.2069674");
                    lugares.Add("merida", "20.969455, -89.623218");
                    lugares.Add("carrillo_puerto", "19.5782387,-88.0486712");
                    lugares.Add("bacalar", "18.6727313,-88.4297951");
                    lugares.Add("chetumal", "18.518812,-88.313586");
                    lugares.Add("xpujil", "18.5084888,-89.3937938");
                    lugares.Add("escarcega", "18.6034742,-90.7518701");
                    lugares.Add("calkini", "20.3699111,-90.0548556");
                    lugares.Add("hecelchakan", "20.178863,-90.148646");
                    lugares.Add("campeche", "19.8233617,-90.5397611");
                    lugares.Add("seiba_playa", "19.6400477,-90.6908389");
                    lugares.Add("champoton", "19.3577941,-90.725149");
                    lugares.Add("sabancuy", "18.9748706,-91.1818179");
                    lugares.Add("candelaria", "18.1878737,-91.0515834");
                    lugares.Add("cd_del_carmen", "18.6450863,-91.8105662");
                    lugares.Add("villa_el_triunfo", "17.9211465,-91.1740492");
                    lugares.Add("balancan", "17.7976965,-91.5342343");
                    lugares.Add("tenosique", "17.4632247,-91.4383094");
                    lugares.Add("emiliano_zapata", "17.7414559,-91.774877");
                    lugares.Add("jonuta", "8.0902265,-92.1403604");
                    lugares.Add("villahermosa", "18.0012991,-92.9228674");
                    lugares.Add("playas_de_catazaja", "18.0012989,-92.9228943");
                    lugares.Add("palenque", "17.50919,-91.9980568");
                    lugares.Add("frontera", "18.530218,-92.6486957");
                    lugares.Add("sayula", "19.8847572,-103.6019828,17");
                    lugares.Add("matias_romero", "16.8798105,-95.0414771,17");
                    lugares.Add("zacapoaxtla", "19.875768,-97.5894296");
                    lugares.Add("coscomatepec", "19.0730099,-97.0462141");
                    lugares.Add("tecamachalco", "18.882461,-97.7375163");
                    lugares.Add("cd_pemex", "17.881881, -92.482155");
                    lugares.Add("herradura", "18.158747, -95.187658");
                    lugares.Add("juan_diaz_c", "18.158297, -95.185571");
                    lugares.Add("huatulco", "15.8327441,-96.3206729");

                    for (int i = 0; i < rutas.Length; i++)
                    {
                        posision = lugares[rutas[i]].IndexOf(",");

                        lista[i, 0] = Convert.ToDouble(lugares[rutas[i]].Substring(0, posision));
                        lista[i, 1] = Convert.ToDouble(lugares[rutas[i]].Substring(posision + 1));
                    }

                    GMapOverlay ruta = new GMapOverlay("CapaRutas");
                    List<PointLatLng> puntos = new List<PointLatLng>();

                    //Agarramos los datos del arreglo
                    for (int i = 0; i < lista.GetLength(0); i++)
                    {
                        puntos.Add(new PointLatLng(lista[i, 0], lista[i, 1]));
                    }

                    GMapRoute PuntosRuta = new GMapRoute(puntos, "Ruta");
                    ruta.Routes.Add(PuntosRuta);
                    gMapControl1.Overlays.Add(ruta);

                    //marcador
                    mapOverlay = new GMapOverlay("Marcador");

                    for (int j = 0; j < lista.GetLength(0); j++)
                    {
                        marker = new GMarkerGoogle(new PointLatLng(lista[j, 0], lista[j, 1]), GMarkerGoogleType.red);
                        mapOverlay.Markers.Add(marker);//Agregamos al mapa
                        //Tooltip
                        marker.ToolTipMode = MarkerTooltipMode.Always;
                        marker.ToolTipText = string.Format(rutas[j]);

                        //Insertamos datos al dt para mostrar la lista
                        dt.Rows.Add(rutas[j], lista[j, 0], lista[j, 1]);
                    }

                    dataGridView1.DataSource = dt;

                    //agregamos el mapa y el marcador al map control
                    gMapControl1.Overlays.Add(mapOverlay);



                    //Actualizamos el mapa
                    gMapControl1.Zoom = gMapControl1.Zoom + 1;
                    gMapControl1.Zoom = gMapControl1.Zoom - 1;
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }

            }
        }
    }
}
